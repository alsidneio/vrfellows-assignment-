using System;
using System.Collections.Generic;

namespace vrFellow
{
    public static class Homework2
    {
        public static void greatest(params int[] args){
            string position = "1st";
            int max = args[0];

            if(args[1]>max){
                max=args[1];
                position="2nd";
            }

            if(args[2]>max){
                max= args[2];
                position = "3rd";    
            }

            Console.WriteLine("The "+position+" is the greatest among the three.");
        }

        public static void evenOdd(int num){
            if(num%2==1){
                Console.WriteLine(num +" is an odd integer");
            }else{
                Console.WriteLine(num +" is an even integer");
            }
        }

        public static void isVowel(char letter){
            var map = new Dictionary<char, Boolean>();
            map.Add('a', true);
            map.Add('e', true);
            map.Add('i', true);
            map.Add('o', true);
            map.Add('u', true);
            
            if(map.ContainsKey(Char.ToLower(letter))){
                Console.WriteLine(letter +" is a vowel");
            }else{
                Console.WriteLine(letter+" is a consonant");
            }
            
        }

        public static void fizzBuzz(){
            for(int i=1; i<=100;i++){
               if(i%15==0){
                   Console.WriteLine(i+": FizzBuzz");
               }else if(i%3==0){
                   Console.WriteLine(i+": Fizz");
               }else if(i%5==0){
                   Console.WriteLine(i+": Buzz");
               }
            }
        }
    }
}